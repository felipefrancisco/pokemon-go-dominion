<?php

/**
 * main page
 */
Route::get('/', 'IndexController@index');

/**
 * compiler route
 */
Route::get('/compile', 'IndexController@compile');

/**
 * data
 */
Route::get('/api/data', 'ApiController@data');

/**
 * data
 */
Route::get('/api/config', 'ApiController@config');