<?php

namespace App\Http\Controllers;

use App\ApiLog;
use App\Exceptions\BlockedIpAddressException;
use App\Location;
use App\Marker;
use App\Services\AuthService;
use App\Services\CacheService;
use App\Services\LocationService;
use App\Services\LoggerService;
use App\Services\MapService;
use App\Services\MarkerService;
use App\Services\NetworkingService;
use App\Services\PokemonService;
use App\Services\UserService;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Ramsey\Uuid\Uuid;

class ApiController extends Controller
{
    /**
     * @var NetworkingService
     */
    protected $networkingService;

    /**
     * @var MapService
     */
    protected $markerService;

    /**
     * @var LoggerService
     */
    protected $loggerService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var CacheService
     */
    protected $cacheService;


    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        # christ, too many injections.
        $this->networkingService = app(NetworkingService::class);
        $this->markerService     = app(MarkerService::class);
        $this->loggerService     = app(LoggerService::class);
        $this->userService       = app(UserService::class);
        $this->cacheService      = app(CacheService::class);
    }

    /**
     * Add/update marker
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $ports = range(5000, 5002);

        $result = [];
        foreach($ports as $port) {

            $response = @json_decode(file_get_contents("http://localhost:$port/data"));

            if(!$response)
                continue;

            foreach($response as &$marker) {

                $marker->radius = 500;
                $marker->fillColor = $this->markerService->getFillColor($marker->icon);

                if($marker->type == 'gym')
                    $this->markerService->save($marker);

                $marker->icon = 'static/square.svg';
            }

            $result = array_merge($result, $response);
        }

        return response()->json($result);
    }


    /**
     * Add/update marker
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function config(Request $request)
    {
        $location = $this->networkingService->fetchLatLngByIp('37.228.248.71');

        $response = [
            'lat' => $location['lat'],
            'lng' => $location['lng'],
            'identifier' => 'map-canvas',
            'zoom' => 16
        ];

        return response()->json($response);
    }
}