<?php
/**
 * Created by PhpStorm.
 * User: Felipe Francisco
 * Date: 07/21/2016
 * Time: 12:36 AM
 */

namespace App\Services;

use App\ApiLog;
use App\Library\Hashing\GibberishAES;
use App\Marker;
use App\Report;
use App\Sight;
use App\User;

class MarkerService
{
    public function save($data) {

        $marker = $this->findOrCreate([
            'lat' => $data->lat,
            'lng' => $data->lng
        ]);

        if($marker->icon != $data->icon);
            $this->fill($marker, $data);

        $marker->save();
    }

    public function findOrCreate($location) {

        $hash = md5($location['lat'] . $location['lng']);

        $marker = Marker::where('hash', $hash)->first();

        if(!$marker)
            $marker = new Marker();

        return $marker;
    }

    public function fill(Marker $marker, \stdClass $data) {

        $marker->icon = $data->icon;
        $marker->infobox = $data->infobox;
        $marker->color = $data->fillColor;
        $marker->lat = $data->lat;
        $marker->lng = $data->lng;
        $marker->key = $data->lat;
        $marker->type = $data->type;

        $marker->hash =  md5($marker->lat . $marker->lng);
    }


    public function getFillColor($icon) {

        $icon = strtolower($icon);

        if(strstr($icon, 'valor') !== false)
            return '#D10F04';

        if(strstr($icon, 'mystic') !== false)
            return '#006AD1';

        if(strstr($icon, 'instinct') !== false)
            return '#D1D000';

        if(strstr($icon, 'harmony') !== false)
            return '#000000';
    }
}